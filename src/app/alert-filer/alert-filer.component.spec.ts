import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlertFilerComponent } from './alert-filer.component';

describe('AlertFilerComponent', () => {
  let component: AlertFilerComponent;
  let fixture: ComponentFixture<AlertFilerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlertFilerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertFilerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
