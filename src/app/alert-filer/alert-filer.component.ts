import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Alert } from '../model';
import { Observable } from 'rxjs';

interface ClientIPs {
  ip: string;
  count: number;
}

@Component({
  selector: 'app-alert-filer',
  templateUrl: './alert-filer.component.html',
  styleUrls: ['./alert-filer.component.css']
})
export class AlertFilerComponent implements OnInit, OnChanges {

  @Input()
  alerts$: Observable<Alert[]>;

  secHeight = 0;
  secMed = 0;
  secLow = 0;

  http = 0;
  ftp = 0;
  tls = 0;

  clientIps: ClientIPs[] = [];

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges (changes: SimpleChanges) {
    if (this.alerts$) {
      this.alerts$.subscribe(alerts => {
        const clientIPs = {};
        alerts.forEach(alert => {
          switch (alert.Severity) {
            case 'High':
              this.secHeight++;
            case 'Medium':
              this.secMed++;
            case 'Low':
              this.secLow++;
          }

          switch (alert.Protocol) {
            case 'HTTP':
              this.http++;
            case 'FTP':
              this.ftp++;
            case 'TLS':
              this.tls++;
          }

          clientIPs[alert.ClientIP] = clientIPs[alert.ClientIP] ? ++clientIPs[alert.ClientIP] : 1;
        });

        for (let c in clientIPs) {
          this.clientIps.push({
            ip: c,
            count: clientIPs[c]
          })
        }
      })
    }

  }

}
