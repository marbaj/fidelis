import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import { Alert } from '../model';

@Component({
  selector: 'app-alerts-table',
  templateUrl: './alerts-table.component.html',
  styleUrls: ['./alerts-table.component.css']
})
export class AlertsTableComponent implements OnInit {

  @Input ()
  alerts$: Observable<Alert[]>;

  @Output()
  onAlert = new EventEmitter<Alert>();

  constructor () { }

  ngOnInit() {
  }

  showAlert (alert) {
    this.onAlert.emit(alert);
  }

}
