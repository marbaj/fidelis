import { Component, OnInit, Input } from '@angular/core';
import { Alert } from '../model';

@Component({
  selector: 'app-alert-detail',
  templateUrl: './alert-detail.component.html',
  styleUrls: ['./alert-detail.component.css']
})
export class AlertDetailComponent implements OnInit {

  @Input ()
  alert;

  alertId: number;


  constructor () { }

  ngOnInit () {
  }

}
