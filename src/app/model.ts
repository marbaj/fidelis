export interface Alert {
    AlertId: Number,
    AlertTime: string,
    Severity: string,
    ClientIP: string;
    ServerIP: string;
    Protocol: string;
    ClientCountry: string;
}
