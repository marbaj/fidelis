import { Component, OnInit } from '@angular/core';
import { DataService } from './data.service';
import { Observable, of } from 'rxjs';
import { Alert } from './model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public alerts$: Observable<Alert[]>;
  public currentAlert: Alert;

  constructor (private dataService$: DataService) {
    this.alerts$ = this.dataService$.load();
  }

  ngOnInit () {
   
  }

  showAlert(alert: Alert) {
    this.currentAlert = alert;
  }

}
