import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AlertsTableComponent } from './alerts-table/alerts-table.component';
import { AlertDetailComponent } from './alert-detail/alert-detail.component';
import { AlertFilerComponent } from './alert-filer/alert-filer.component';

@NgModule({
  declarations: [
    AppComponent,
    AlertsTableComponent,
    AlertDetailComponent,
    AlertFilerComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
