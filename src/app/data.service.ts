
import { Injectable } from '@angular/core';
import data from './data';
import { Observable, of } from 'rxjs';
import { Alert } from './model';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor() { }

  load (): Observable<Alert[]> {

    const alerts: Alert[] = data;

    return of(alerts);
  }
}
